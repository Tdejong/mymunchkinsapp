package com.example.munchkins;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.munchkins.player.PlayerAdapter;
import com.example.munchkins.player.Players;

import maes.tech.intentanim.CustomIntent;

public class TheGame extends AppCompatActivity {

    private ListView view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_game);

        view = findViewById(R.id.playerListView);

        generateData();

    }

    private void generateData(){
        PlayerAdapter playerAdapter = new PlayerAdapter(this, Players.getPlayers());

        view.setAdapter(playerAdapter);
    }

    @Override
    public void finish() {
        super.finish();
        CustomIntent.customType(this, "right-to-left");
    }
}