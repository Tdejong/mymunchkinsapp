package com.example.munchkins.customViews;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.munchkins.R;

public class LvlBonusView extends LinearLayout {

    private TextView textView;
    private Button minButton, addButton;

    public LvlBonusView(Context context) {
        super(context);
        intit();
    }

    public LvlBonusView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        intit();
    }

    public LvlBonusView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        intit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LvlBonusView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        intit();
    }

    public void intit(){
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.lvl_bonus_view, this);

        textView = findViewById(R.id.strengthView);
        minButton = findViewById(R.id.minButton);
        addButton = findViewById(R.id.addButton);
    }

    public void setStrength(){
        int strength = 1;
        this.textView.setText("" + strength);

    }
}
