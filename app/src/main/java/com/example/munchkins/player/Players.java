package com.example.munchkins.player;

import java.util.ArrayList;

public class Players {

    private static ArrayList<Player> players;

    static {
        players = new ArrayList<>();
        players.add(new Player("tim", 1, 0));
        players.add(new Player("aron", 1, 0));
        players.add(new Player("jefrey", 1, 0));
    }

    public static ArrayList<Player> getPlayers() {
        return players;
    }

}
