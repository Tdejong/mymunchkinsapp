package com.example.munchkins.player;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.munchkins.R;
import com.example.munchkins.customViews.LvlBonusView;

import java.util.List;

public class PlayerAdapter extends ArrayAdapter<Player> {
    public PlayerAdapter(@NonNull Context context,  @NonNull List<Player> players) {
        super(context, 0, players);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View playerView = convertView;

        if (playerView == null){
            playerView= LayoutInflater.from(getContext()).inflate(R.layout.player_compound, parent, false);
        }
        Player player = getItem(position);

        TextView playerName = playerView.findViewById(R.id.playerName);
        LvlBonusView level = playerView.findViewById(R.id.playerLevel);
        LvlBonusView bonus = playerView.findViewById(R.id.playerBonus);

        playerName.setText(player.getName());

        //todo make it so that that the level can be upped and lowered

        return playerView;
    }
}
