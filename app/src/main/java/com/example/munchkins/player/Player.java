package com.example.munchkins.player;

/**
 * the player model, a player has a name so he/she can be identified a lvl
 * which if 10 will end the game.
 * and lastly a integer to keep track of the strength of the player
 *
 */
public class Player {

    private String name;
    private int level;
    private int bonusses;

    public Player(String name, int level, int bonusses) {
        this.name = name;
        this.level = level;
        this.bonusses = bonusses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getBonusses() {
        return bonusses;
    }

    public void setBonusses(int bonusses) {
        this.bonusses = bonusses;
    }
}
