package com.example.munchkins;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.munchkins.discussion.DiscussionActivity;
import com.example.munchkins.player.AddNewPlayer;
import com.example.munchkins.player.Player;

import maes.tech.intentanim.CustomIntent;

public class MainActivity extends AppCompatActivity {

    private Button startButton, addNewPlayerButton, leaderBoardButton, discussionsButton;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        startButton = findViewById(R.id.startButton);
        addNewPlayerButton = findViewById(R.id.addNewPlayerButton);
        discussionsButton = findViewById(R.id.discussionsButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame();
            }
        });

        addNewPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewPlayer();
            }
        });

        discussionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDiscussions();
            }
        });
    }

    public void startGame(){
        Intent startIntent = new Intent(this, TheGame.class);
        startActivity(startIntent);
        CustomIntent.customType(this, "left-to-right");
    }

    public void addNewPlayer(){
        Intent AddNewPlayerIntent = new Intent(this, AddNewPlayer.class);
        startActivity(AddNewPlayerIntent);
        CustomIntent.customType(this, "left-to-right");
    }

    public void goToDiscussions(){
        Intent goToDiscussionsIntent = new Intent(this, DiscussionActivity.class);
        startActivity(goToDiscussionsIntent);
        CustomIntent.customType(this, "left-to-right");
    }

}