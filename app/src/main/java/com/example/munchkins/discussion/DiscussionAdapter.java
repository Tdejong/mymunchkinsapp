package com.example.munchkins.discussion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.munchkins.R;

import java.util.List;

public class DiscussionAdapter extends ArrayAdapter<Discussion> {
    public DiscussionAdapter(@NonNull Context context, @NonNull List<Discussion> discussions) {
        super(context, 0, discussions);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View discussionView = convertView;

        if (discussionView == null){
            discussionView = LayoutInflater.from(getContext()).inflate(R.layout.discussion_compound, parent, false);
        }
        Discussion discussion = getItem(position);
        System.out.println(discussion.getCard());

        TextView discussionTitle = discussionView.findViewById(R.id.discussionName);
        TextView discussionExplanation = discussionView.findViewById(R.id.discussionExplanation);
        TextView discussionResult = discussionView.findViewById(R.id.discussionResult);

        discussionTitle.setText(discussion.getIssue());
        discussionExplanation.setText(discussion.getExplanation());
        discussionResult.setText(discussion.getResult());

        return discussionView;
    }
}
