package com.example.munchkins.discussion;

/**
 * a discussion happens when the players disagree about the interpretation of a card
 * keeping track of these discussions helps to make the game more equal across multiple
 * games
 */

public class Discussion {
    private String card, issue, explanation, result;


    public Discussion(String card, String issue, String explanation, String result) {
        this.card = card;
        this.issue = issue;
        this.explanation = explanation;
        this.result = result;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
