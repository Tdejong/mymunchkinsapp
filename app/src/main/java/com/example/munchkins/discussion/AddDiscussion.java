 package com.example.munchkins.discussion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.munchkins.R;

import maes.tech.intentanim.CustomIntent;

 public class AddDiscussion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_discussion);
    }

     @Override
     public void finish() {
         super.finish();
         CustomIntent.customType(this, "right-to-left");
     }
 }