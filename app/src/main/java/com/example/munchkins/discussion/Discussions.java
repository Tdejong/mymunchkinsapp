package com.example.munchkins.discussion;

import java.util.ArrayList;

public class Discussions {

    private static ArrayList<Discussion> discussions;

    static {
        discussions = new ArrayList<>();

        discussions.add(new Discussion("wizard class", "can a wizard enchant an monster he isnt fighting",
                "aron has been enchanting monsters that he isnt fighting and the card doesnt explain clearly enough", "wizard cannot fight monsters that they them self are not fighting"));

        discussions.add(new Discussion("thief class", "can a lvl 1 thief keep steeling from other players without consequence",
                "aron has been steeling from us as a lvl 1 thief without much consequence and it breaking the game", "a lvl 1 thief may not steal"));
    }

    public static ArrayList<Discussion> getDiscussions() {
        return discussions;
    }
}
