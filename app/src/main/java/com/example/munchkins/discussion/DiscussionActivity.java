package com.example.munchkins.discussion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.munchkins.R;
import com.example.munchkins.player.PlayerAdapter;
import com.example.munchkins.player.Players;

import maes.tech.intentanim.CustomIntent;

public class DiscussionActivity extends AppCompatActivity {

    private ListView view;
    private Button addDiscussionBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion);

        view = findViewById(R.id.discussionListView);
        addDiscussionBtn = findViewById(R.id.addDiscussionBtn);

        addDiscussionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDiscussion();
            }
        });

        generateData();
    }

    private void generateData(){
        DiscussionAdapter discussionAdapter = new DiscussionAdapter(this, Discussions.getDiscussions());

        view.setAdapter(discussionAdapter);
    }

    private void addDiscussion(){
        Intent addDiscussionIntent = new Intent(this, AddDiscussion.class);
        startActivity(addDiscussionIntent);
        CustomIntent.customType(this, "left-to-right");    }

    @Override
    public void finish() {
        super.finish();
        CustomIntent.customType(this, "right-to-left");
    }
}